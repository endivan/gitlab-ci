GitLab CI/CD Catalog, Components and Templates
==============================================

**GitLab CI/CD CCT** is a comprehensive **GitLab** CI/CD YAML template repository.

It's existence is to simplify and streamline your continuous integration and continuous deployment pipelines for Python projects. We understand that every project is unique, and managing complex CI configurations can be time-consuming. Our goal is to provide you with a flexible, one-size-fits-most solution that can be easily customized to meet your specific project requirements.

.. epigraph::

  *One size fits most.*

.. toctree::
   :maxdepth: 1
   :caption: Introduction

   introduction.rst
   usage.rst

.. toctree::
   :maxdepth: 1
   :caption: Stages, Workflows, & Core files

   stages.rst
   workflow.rst
   core.rst

   docker.rst
   misc.rst
   python.rst

.. toctree::
   :maxdepth: 1
   :caption: Catalog jobs

   docker-jobs.rst
   misc-jobs.rst

.. toctree::
   :maxdepth: 1
   :caption: Miscellaneous

   caveats.rst
   license.rst
