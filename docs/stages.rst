Stages
======

.. include:: shared/local-toc.rst

Usage
-----

.. code-block:: yaml

  include:
    - component: "${CI_CATALOG_PATH}/templates/stages@${CI_CATALOG_REF}"


Default stages
--------------

.. templates/stages/template.yml

Continuous Integration (CI) pipelines typically involve a series of stages and steps to automate the development, testing, and deployment processes.

GitLab CI defaults to 5 stages if nothing is specified.

Meanwhile, this project's default stages are the `GitLab stages`_ expanded with a :em:`pre-` and :em:`post-` stage. In addition, new stages such as :em:`lint` and :em:`coverage` exist.

.. _GitLab stages: https://docs.gitlab.com/ee/ci/yaml/#stages

* :em:`.pre`: Predefined GitLab default stage.

* :em:`pre-lint`
* :hi:`lint`: This stage involves using tools to analyze the codebase for potential issues, such as code style violations.
* :em:`post-lint`

* :em:`pre-coverage`
* :hi:`coverage`: Calculate code coverage metrics to ensure that a sufficient percentage of the codebase is covered by automated tests.
* :em:`post-coverage`

* :em:`pre-build`
* :hi:`build`: Predefined GitLab default stage.
* :em:`post-build`

* :em:`pre-test`
* :hi:`test`: Predefined GitLab default stage.
* :em:`post-test`

* :em:`pre-release`
* :hi:`release`: Automatically generate release notes or changelogs based on code changes and commits in version control.
* :em:`post-release`

* :em:`pre-deploy`
* :hi:`deploy`: Predefined GitLab default stage.
* :em:`post-deploy`

* :em:`pre-trigger`
* :hi:`trigger`
* :em:`post-trigger`

* :em:`.post`: Predefined GitLab default stage.
