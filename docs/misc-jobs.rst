Misc jobs
=========

.. include:: shared/local-toc.rst

misc:sphinx:html
----------------

Build Sphinx documentation. Automatically runs when :file:`docs/conf.py` is detected.

**pipelines**: all
**script_command**: ``make html``
**default_rule**: ``never``

Spec inputs
^^^^^^^^^^^

* :ev:`default_rule`: Enable/Disable the job. Allowed values: ``never``, ``on_success``, ``always``, ``manual``. Interpreted as the last rule. Default: ``never``
* :ev:`command_script`: Script to execute. Defaults to ``make html``
* :ev:`requirement_files`: Space separated list of ``requirements.txt`` type files. Only the first found file will be used.


pages
-----

Job groups all exposed artifacts into one and publishs to GitLab pages.

Spec inputs
^^^^^^^^^^^

* :ev:`enable`: Enable/Disable the job. Allowed values: ``never``, ``on_success``, ``always``, ``manual``. Interpreted as the last rule. Default: ``always``

Conditions
^^^^^^^^^^

* GitLab pages must be active.
* Branch must be protected.
* Branch must be default branch (usually `master` or `main`)
