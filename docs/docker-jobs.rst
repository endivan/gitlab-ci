Docker jobs
===========

.. include:: shared/local-toc.rst

docker:build
------------

Job executes ``make push`` by default. Requires GitLab registry to be active for the project. Nevertheless, GitLab registry doesn't have to point to GitLab's builtin registry.

**pipelines**: all
**script_command**: ``make html``
**default_rule**: ``always``

Spec inputs
^^^^^^^^^^^

* :ev:`enable`: Enable/Disable the job. Allowed values: ``never``, ``on_success``, ``always``, ``manual``. Interpreted as the last rule. Default: ``always``
* :ev:`job_name`: Job name. Defaults to ``docker:build``
* :ev:`command_script`: Script to execute. Defaults to ``make push``


Conditions
^^^^^^^^^^

* Registry must be active.


Usage
^^^^^

.. code-block:: yaml

  include:
    - component: "${CI_CATALOG_PATH}/templates/docker@${CI_CATALOG_REF}"
    - component: "${CI_CATALOG_PATH}/templates/jobs/docker@${CI_CATALOG_REF}"
