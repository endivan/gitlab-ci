# Build only sphinx-html documentation
SPHINXBUILDFLAGS ?=

.PHONY: all
all: html

.PHONY: html docs doc
html docs doc: ## build html documentation

.PHONY: sphinx-html
html docs: sphinx-html
sphinx-html: public docs/conf.py
	@PYTHONPATH=".:$$PYTHONPATH" sphinx-build -b html -t html $(SPHINXBUILDFLAGS) docs ./public/sphinx/html ; \
	echo "Documentation generated here: file://$(shell pwd)/public/sphinx/html/index.html"

clean: sphinx-html-clean
sphinx-html-clean:
	@rm -fr public/sphinx/html
	@-if test -d public/sphinx; then rmdir public/sphinx; fi

docs/conf.py:
